#!/usr/bin/perl -w

=head1 NAME

dh_installlisting - install .listing files to be used by desktop-profiles package

=cut

use strict;
use Debian::Debhelper::Dh_Lib;

=head1 SYNOPSIS

B<dh_installlisting> [S<B<debhelper options>>] [S<B<filename(s)>>]

=head1 DESCRIPTION

dh_installlisting is a debhelper program that handles installing listing files
used by the desktop-profiles package into the correct location in package
builddirectories (NOTE: this command is provided by the desktop-profiles 
package, so don't forget to build-depends on it). It also updates the cache
of profile assignments (when it exists) to reflect the added metadata.

If a file named debian/package.listing exists (or debian/listing in case of the 
main package) it is installed in etc/desktop-profiles. In addition any files 
given as argument will be installed in etc/desktop-profiles as 
package_file.listing. The format of .listing files is described in 
L<desktop-profiles(7)>.

A dependency on desktop-profiles will be added to misc:Depends by using this
script.

=cut

init();

foreach my $package (@{$dh{DOPACKAGES}}) {
  my $tmp=tmpdir($package);
	
  # Add the debian/listing (or debian/$package.listing) if present
  my $debDirListing=pkgfile($package,"listing");
  my @listings=$debDirListing;
  my $containsGNUSTEP='false';

  # Add further listing files given as arguments if present
  if (($package eq $dh{FIRSTPACKAGE} || $dh{PARAMS_ALL}) && @ARGV) {
	push @listings, @ARGV;
  }
	
  # if we have listings to install
  if ("@listings" ne '') {
    # make sure the directory we need exists
    if (! -d "$tmp/etc/desktop-profiles") {
      doit("install","-d","$tmp/etc/desktop-profiles");
    }

    # install each listing file 
    # (making sure not to double '.listing' nor the packagename)
    foreach my $file (@listings) {
      my $installName=basename($file);
      $installName=~s/(.*)\.listing$/$1/;
      if ( ("$installName" ne 'listing') && ("$installName" ne "$package") ){
        $installName=~s/^/_/;
        doit("install","-p","-m644",$file,"$tmp/etc/desktop-profiles/$package$installName.listing");
      } else {
        doit("install","-p","-m644",$file,"$tmp/etc/desktop-profiles/$package.listing");
      }
      
    }
    # if we're not installing desktop-profiles itself, the autoscript isn't yet present
    if ( "$package" ne "desktop-profiles") {
      # Make sure the postinst regenerates runs /usr/bin/update-profile-cache
      # to ensure en up-to-date cache of profile assignments
      autoscript("$package","postinst","postinst-desktop-profiles");
    }  
  }  
	
  # Add desktop-profiles to the dependencies if necesary
  if ($package ne 'desktop-profiles') {
    addsubstvar($package, "misc:Depends", "desktop-profiles (>= 1.4)");
  }
}

=head1 SEE ALSO

L<debhelper(7)>
L<desktop-profiles(7)>
L<update-profile-cache(1)>

=head1 AUTHOR

Bart Cornelis  (cobaco) <cobaco@skolelinux.no>

=cut
