GENERAL DESCRIPTION
=-=-=-=-=-=-=-=-=-=
See the desktop-profiles(7) man page for a description of how this package 
works, and what it does.

KNOWN BUGS
=-=-=-=-=-=
- 'ssh -X' bug (##344030): 
  Profiles aren't set when logging in and running programs with 'ssh -X', this
  is because the Xsession.d script isn't run.
  
  A general shell-independ solution seems to be impossible (neither ssh itself
  nor PAM provides a way to source shell scripts, if you have any ideas please
  contact me). 
  Shell-variant specific solutions are possible by having the system-wide 
  on-logon script of each shell run the profile activation script when an 
  'ssh-X' login is detected. 
  This package currently documents the necessary bits of code for the following
  shells: bash, dash, ksh, pdksh, mksh, csh, tcsh, zsh, zoidberg, fish and psh. 
  Where possible the fix is applied automatically (this needs a modularized 
  on-logon script), otherwise the admin needs to add the required code by hand.

  For bourne-compatible shells (bash, dash, ksh, pdksh, mksh, posh) the 
  system-wide on-logon script is /etc/profile. The file 
  /usr/share/doc/desktop-profiles/examples/profile-snippet contains the code
  that needs to be added to it in order to fix this bug.

  For the zsh shell the system-wide on-logon script is /etc/zsh/zlogin. The file 
  /usr/share/doc/desktop-profiles/examples/zlogin-snippet contains the
  code-snippet that needs to be added to it in order to fix this bug.

  Users of the csh, fish, psh (>=1.8-6), and tcsh (>=6.14.00-5) and zoidberg 
  (>= 0.96-1) shells don't need to do anything as the system-wide on-logon 
  script of these shells is modularized, and thus the required code-snippet is
  dropped into place on package installation.

PERFORMANCE
=-=-=-=-=-=
For those wondering about performance, I did some benchmarking of the Xsession.d
script yielding the following results[NOTE]:

- needed execution time of the script when dynamicaly checking which profiles 
  should be activated is linear in respect to the number of profiles 
  (i.e. O(n) with n = #profiles), tested up to a 100 profiles.

  * dynamic activation with /bin/sh pointing to dash (averaged over 10 runs)
			out-of-the-box	20	30	40	50	60
  pentium2 350 Mhz:		0.594	0.980	1.168	1.369	1.557	1.751
  celeron M 1.4 Ghz:		0.100	0.140	0.170	0.212	0.248	0.284	
  centrino duo 1.83 GHz:	0.080	0.121	0.150	0.188	0.220	0.254

  * dynamic activation with /bin/sh pointing to bash (averaged over 10 runs)
			out-of-the-box	20	30	40	50	60
  pentium2 350 Mhz:		0.983	1.534	1.878	2.236	2.585	2.944
  celeron M 1.4 Ghz:		0.163	0.241	0.316	0.387	0.460	0.530
  centrino duo 1.83 GHz:	0.153	0.222	0.290	0.359	0.429	0.494

  -> performance when /bin/sh points to dash about is 40-45% faster as when it 
     points to bash.
  
- needed execution time in the simple case with an up-to-date cache is constant 
  and so small as to be negligable even on old systems:

   * with up-to-date cache 
     				/bin/sh -> dash		/bin/sh -> bash
     pentium2 350 Mhz:		0.010			0.092
     celeron M 1.4 Ghz:		0 (to small to measure)	0.010 
     centrino duo 1.83 Ghz:	0 (to small to measure) 0.010

- Here's some more info about the machines I ran the test on:  
   pentium2 350 Mhz -> 128 MB RAM, Quantum Fireball Ex4.3A HD
   celeron 1.4 Ghz -> Acer Travelmate 2312LM, 256 MB RAM, HTS726060M9AT00 HD
   centrino duo 1.83 Ghz -> HP nx9420, 512 MB RAM, FUJITSU MHV2100B HD

NOTE: For those that want to run the performance tests on their own systems
      the testscript and metadata-sets I used can be found in the tests.tgz file
      in /usr/share/doc/desktop-profiles. The test script in there needs to be
      run as a user with write priviliges to the directory /etc/desktop-profiles
      and will test performance with both dash (if present) and bash and up-to 
      100 profiles.

GCONF PROFILES 
=-=-=-=-=-=-=-=
The default gconf systemwide path file provided by the gconf2 package only
includes the minimal changes necessary for using desktop-profiles. That is 
it will only add in the configuration sources managed by desktop-profiles, 
it won't put all configuration sources under desktop-profiles control.

To facilitate those that want to manage all their gconf configuration sources
with desktop-profiles this package provides a conversion script 
(/usr/sbin/path2listing) that will parse your current systemwide pathfile, 
generate the necessary metadata for all your currently used configuration 
sources (yes it will recurse into included path files), and change the 
systemwide gconf path file to reflect that all configuration sources will now
be managed by desktop-profiles. 

Running the conversion script doesn't result in any user-visible changes (if it 
does there's a bug), still the script will make a backup of the current path 
file before changing it so you can always go back to not managing the gconf 
configuration sources with desktop-profiles by simply restoring the path file
backup.

