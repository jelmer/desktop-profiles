#!/usr/bin/make -f

DESTDIR =
PREFIX = /usr

BUILD_DATE = $(shell dpkg-parsechangelog --show-field Date)

build: documentation check

documentation: dh_installlisting.1
dh_installlisting.1: dh_installlisting
	pod2man dh_installlisting > dh_installlisting.1

install:
	./dh_installlisting

zip-tests:
	find tests -not -type d -print0 | \
		LC_ALL="C" sort --zero-terminated | \
		GZIP="-9n" tar --mode=go=rX,u+rw,a-s --create --gzip --null --files-from=- \
			--file=tests.tgz --mtime="$(BUILD_DATE)" \
			--owner=root --group=root --numeric-owner

check: zip-tests

clean:
	if(test -e tests.tgz); then rm tests.tgz; fi

distclean: clean
realclean: distclean
	if(test -e dh_installlisting.1); then rm dh_installlisting.1; fi;
